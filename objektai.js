

var masinos = [
    ["2019", "LRS 123", 100, 50],
    ["2018", "LRS 321", 30, 40],
    ["2017", "ABC 123", 10550, 50],
];
masinos[2][1]; // ABC 123


var masinos1 = [
    {data: '2019', numeris: 'LRS 123', greitis: 100, laikas: 50},
    {data: '2018', numeris: 'LRS 123', greitis: 100, laikas: 50},
    {data: '2017', numeris: 'ABC 123', greitis: 10550, laikas: 50}
];

masinos1[2].numeris; // ABC 123


var zmogus = {
    vardas: "testas", // 32
    pavarde: "testauskas",
    adresas: {
        salis: "Lietuva",
        miestas: "Kaunas",
        gatve: 'Jaksto 8'
    },
    pazymiai : [10, 5, 15, 2],
    masinos: [
        {data: '2019', numeris: 'LRS 123', greitis: 100, laikas: 50},
        {data: '2018', numeris: 'LRS 123', greitis: 100, laikas: 50},
        {data: '2017', numeris: 'ABC 123', greitis: 10550, laikas: 50}
    ],
    skaiciuotiVidurki: function() {
        alert("cia kazka skaiciuosiu");
    }
    
}
zmogus.adresas.gatve;

console.log(zmogus);
zmogus.skaiciuotiVidurki();

var suma = 0;

for(var i = 0; i< zmogus.pazymiai.length; i++) {
    suma += zmogus.pazymiai[i];
}

zmogus.vardas = suma;